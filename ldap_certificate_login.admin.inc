<?php

/**
 * @file
 * Administrative page callbacks for the ldap_certificate_login module
 */

/**
 * form for updating settings for loggin in via certificate
 *
 * @param <type> $form
 * @param <type> $form_state
 * @return array drupal form array
 */
function ldap_certificate_login_admin_form($form, &$form_state) {
  // have placeholder for tokens in case it is used at a later date
  $tokens = array();

  $form['intro'] = array(
    '#type' => 'item',
    '#markup' => t('<h1>LDAP Certificate Login Settings</h1>'),
  );
  
  $form['ldap_certificate_login_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => 'Use certificate based user login',
    '#description' => t('Check this box if you would like to use certificates to log into the site instead of a username/password'),
    '#default_value' => variable_get('ldap_certificate_login_enabled',array()),
  );
  
  $form['ldap_certificate_login_cert'] = array(
    '#type' => 'textfield',
    '#title' => 'LDAP certificate field',
    '#description' => t('This is the variable name in ldap that contains the certificate information'),
    '#default_value' => variable_get('ldap_certificate_login_cert',''),
  );

  $form['submit_form'] = array( 
    '#type' => 'submit',
    '#weight' => 10,
    '#value' => t('Save'),
  );

  $form['#validate'][] = 'ldap_certificate_login_form_validate';
  $form['#submit'][] = 'ldap_certificate_login_form_submit';
  return $form;
}


/**
 * Validation handler for ldap_certificate_login admin page.
 */
function ldap_certificate_login_form_validate($form, &$form_state) {
  if(empty($form_state['values']['ldap_certificate_login_cert']) && !$form_state['values']['ldap_certificate_login_enabled']) {
    form_set_error('ldap_certificate_login_enabled', t('You must provide a certificate field to enable certificate login.'));
  }

}

/**
 * Form submission handler for ldap_certificate_login admin settings.
 */
function ldap_certificate_login_form_submit($form, &$form_state) {

  $cur_enable = variable_get('ldap_certificate_login_enabled', 0);
  $cur_cert = variable_get('ldap_certificate_login_cert', '');

  if(!empty($form_state['values']['ldap_certificate_login_cert']) && $form_state['values']['ldap_certificate_login_cert'] != $cur_cert) {
    variable_set('ldap_certificate_login_cert', $form_state['values']['ldap_certificate_login_cert']);
  }
  if(!empty($form_state['values']['ldap_certificate_login_enabled']) && $form_state['values']['ldap_certificate_login_enabled'] != $cur_enable) {
    variable_set('ldap_certificate_login_enabled', $form_state['values']['ldap_certificate_login_enabled']);
  }
  drupal_set_message('Configuration has been saved.');
}
